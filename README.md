# Oracle Extractor

## Description

Extractor for Oracle databases.

##### What it is used for?

Extracting the data from any Oracle Database to the specific folder on the local machine.

##### What the code does?

The code - especially main.py file does:

1. Load the config file, where are all necessary information for the extractor.
2. Create the connection between the machine, where is the script running and the remote database.
3. Ask for the specific data and save them to a specific folder on the local machine as CSV (everything is specified in
   the config.schema.json file - read on).

## Requirements

* python3.6 or higher
* installed oracle client https://cx-oracle.readthedocs.io/en/latest/user_guide/installation.html#

## How to use it?

### Configuration file
See example `config.sample.json` you can find out more in json schema `config.schema.json`

### Local use

1) Create or modify the config.json file (see example `config.sample.json`):
    * config.json = configuration:
    * before running the script fill the config.json file:
        - field "query" options: could contain valid sql selects

2) Run the /scr/main.py
      ```sh
      python3 main.py
      ```

#### script parameters
 * --config \<path to config file\>
 * --output \<path to output directory>
 * --config-schema ../config.schema.json (path to json schema file)
