import csv
from typing import Optional
import cx_Oracle
import logging

from retry_helper import RetryManager

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def decode_bytes(value):
    if isinstance(value, bytes):
        try:
            return value.decode("utf-8")
        except UnicodeDecodeError:
            logger.warning(f"Value {value} cannot be decoded properly it is not valid utf8")
            return value.decode("utf-8", errors="replace")
    return value


def return_strings_as_bytes(cursor, name, default_type, size, precision, scale):
    if default_type == cx_Oracle.DB_TYPE_VARCHAR:
        return cursor.var(str, arraysize=cursor.arraysize, bypass_decode=True)


class OracleExtractor:
    def __init__(
            self,
            user: str,
            password: str,
            host: str,
            sid: str,
            port=1521,
            batch_size: Optional[int] = None,
            max_retries: Optional[int] = None,
            retry_waiting_time: Optional[int] = None,
    ):
        self.user = user
        self.password = password
        self.dsn = cx_Oracle.makedsn(host, port, sid)
        self.batch_size = batch_size or 10000
        self.max_retries = max_retries or 10
        self.retry_waiting_time = retry_waiting_time or 10
        self._connection = None
        self._cursor = None

    def __del__(self):
        self.close_cursor_and_connection()

    @property
    def connection(self):
        if self._connection is None:
            self._connection = cx_Oracle.connect(
                user=self.user, password=self.password, dsn=self.dsn, encoding="UTF-8"
            )
        return self._connection

    @property
    def cursor(self):
        if self._cursor is None:
            self._cursor = self.connection.cursor()
            self._cursor.outputtypehandler = return_strings_as_bytes
        return self._cursor

    def close_cursor_and_connection(self):
        if self._cursor:
            self._cursor.close()
        if self._connection:
            self._connection.close()
            self._connection = None

    def extract_data(self, query: str, file_path):
        with RetryManager(
                max_attempts=self.max_retries,
                wait_seconds=self.retry_waiting_time,
                exceptions=(cx_Oracle.OperationalError, cx_Oracle.InternalError),
                reset_func=self.close_cursor_and_connection,
        ) as retry:
            while retry:
                with retry.attempt:
                    logger.info(f"Writing data to {file_path}")
                    with open(file_path, "w", newline="", encoding="utf-8") as file:
                        writer = csv.writer(file, dialect=csv.unix_dialect)
                        writer.writerows(self._get_all_rows(query))
                    logger.info(f"Data successfully written to {file_path}")

    def _get_all_rows(self, query):
        """
        Yield all rows from query result - first row is columns names
        """
        logger.info(f"Running query: {query}")
        self.cursor.execute(query)
        fieldnames = [column[0] for column in self.cursor.description]
        yield fieldnames
        logger.info(f"Fieldnames: {fieldnames}")
        while True:
            rows = self.cursor.fetchmany(self.batch_size)
            if not rows:
                break
            for row in rows:
                yield (decode_bytes(value) for value in row)
            logger.info(f"Already get {self.cursor.rowcount} rows")
