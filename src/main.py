import os

from bizztreat_base.config import Config

from extractor import OracleExtractor


def main():
    conf = Config(force_schema=True)
    extractor = OracleExtractor(
        user=conf["user"],
        password=conf["password"],
        host=conf["host"],
        sid=conf["sid"],
        batch_size=conf.get("batch_size"),
        max_retries=conf.get("max_retries"),
        retry_waiting_time=conf.get("retry_waiting_time"),
    )
    for table, query in conf["query"].items():
        file_path = os.path.join(conf.output_folder, f"{table}.csv")
        extractor.extract_data(query, file_path)


if __name__ == "__main__":
    main()
