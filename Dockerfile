FROM python:3.9-slim

LABEL com.bizztreat.type="Extractor"
LABEL com.bizztreat.purpose="Bizzflow"
LABEL com.bizztreat.component="ex-oracle"
LABEL com.bizztreat.title="Oracle DB Extractor"

WORKDIR    /opt/oracle
RUN apt-get update && apt-get install -y libaio1 wget unzip \
    && wget https://download.oracle.com/otn_software/linux/instantclient/211000/instantclient-basiclite-linux.x64-21.1.0.0.0.zip \
    && unzip instantclient-basiclite-linux.x64-21.1.0.0.0.zip \
    && rm -f instantclient-basiclite-linux.x64-21.1.0.0.0.zip \
    && rm -f instantclient-basiclite-linux.x64-21.1.0.0.0.zip \
    && sh -c "echo /opt/oracle/instantclient_21_1 > /etc/ld.so.conf.d/oracle-instantclient.conf" \
    && ldconfig

WORKDIR /

ADD requirements.txt .
RUN pip install -r requirements.txt

ADD src/ /code
ADD config.schema.json /code

WORKDIR /code

CMD ["python3", "-u", "main.py"]
